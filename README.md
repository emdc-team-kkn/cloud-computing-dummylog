#How to use
## Installing dependencies

Change to the project directory and run

    bundle install

## Generate dummy log file

Change to the project directory and run

    rake OUT=<file_path> SIZE=<number_of_line_per_file> REPEAT=<number_of_file>
