require 'date'
require 'faker'

class DataGen
  attr_accessor :station_per_region, :region_names,
                :bird_per_type, :bird_types,
                :weight_range,
                :wing_span_range,
                :weather_condition

  def initialize
    @station_per_region = 10
    @region_names = [
        :Alcochete, :Almada, :Amadora, :Barreiro, :Cascais, :Lisboa, :Loures,
        :Mafra, :Moita, :Montijo, :Odivelas, :Oeiras, :Palmela, :Seixal,
        :Sesimbra, :Setubal, :Sintra
    ]
    @bird_per_type = 50
    @bird_types = [
        :eagle, :hawk, :swallow, :seagulf, :falcon
    ]
    @weight_range = 1..10
    @wing_span_range = 50..300
    @weather_condition = [0, 1, 2, 3]
    @start_date = Date.new(2013,1,1)
    @end_date = Date.new(2014,12,31)
  end

  def gen_row
    tower_id = "#{@region_names.sample}-#{rand(@station_per_region)}"
    t = Faker::Time.between(@start_date, @end_date)
    bird_num = rand(-1..@bird_per_type)
    if bird_num != -1 && bird_num != 0
      bird = "#{@bird_types.sample}-#{bird_num}"
      row = "#{tower_id}, #{t.strftime('%Y-%m-%d')}, #{t.strftime('%H:%M:%S')}, " \
        "#{bird}, #{rand(@weight_range)}, #{rand(@wing_span_range)}, " \
        "#{@weather_condition.sample}"
    elsif bird_num == 0
      row = "#{tower_id}, #{t.strftime('%Y-%m-%d')}, #{t.strftime('%H:%M:%S')}, " \
        "#{bird_num}, #{rand(@weight_range)}, #{rand(@wing_span_range)}, " \
        "#{@weather_condition.sample}"
    else
      row = "#{tower_id}, #{t.strftime('%Y-%m-%d')}, #{t.strftime('%H:%M:%S')}, " \
        "-1, 0, 0, #{@weather_condition.sample}"
    end
    row
  end
end